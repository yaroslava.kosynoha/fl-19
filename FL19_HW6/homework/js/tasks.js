const arr = ['1', '3', '4', '2', '5'];
function getMaxElement(array) {
    const arr = array.map(Number);
    const filtered = arr.filter(function(num) {
 return num % 2 === 0; 
});
    return Math.max.apply(null, filtered);
}
console.log(getMaxElement(arr));


let a = 3;
let b = 5;
[a, b] = [b, a];
console.log(a);
console.log(b);


function getValue (value) {
    return value ?? '-'; 
}
console.log(getValue(0));
console.log(getValue(4));
console.log(getValue('someText'));
console.log(getValue(null));
console.log(getValue(undefined));


function getObjFromArray (arr) {
    return Object.fromEntries(arr);
}
const arrayOfArrays = [
    ['name', 'dan'],
    ['age', '21'],
    ['city', 'lviv']
];
console.log(getObjFromArray(arrayOfArrays));


function addUniqueId(array) {
    return {...array, id: Symbol()}
}
const obj1 = { name: 'nick' };
console.log(addUniqueId(obj1));
console.log(addUniqueId({ name: 'buffy'}));
console.log(Object.keys(obj1).includes('id'));


const oldObj = {
    name: 'willow',
    details: { id: 1, age: 47, university: 'LNU'}
};
function getRegroupedObject({details:{university}, details:user, details:{age}, name, details:{id}}) {
    user = {
        age,
        name,
        id
    };
    return {university, user};
}
console.log(getRegroupedObject(oldObj));


function getArrayWithUniqueElements(array) {
    return [...new Set(array)];
}
const arr1 = [2, 3, 4, 2, 4, 'a', 'c', 'a'];
console.log(getArrayWithUniqueElements(arr1))


function hideNumber(number) {
    const end = number.slice(-4);
    return end.padStart(number.length, '*')
}
const phoneNumber = '0123456789';
console.log(hideNumber(phoneNumber));


function add (a, b) {
    if (a > 0 && b > 0) {
        return a + b;
    } else {
        throw new Error('b is required')
    }
}
console.log(add(2, 3));


function* generateIterableSequence() {
    yield 'I'
    yield 'love'
    yield 'EPAM'
}
const generatorObject = generateIterableSequence();
for (let value of generatorObject) {
    console.log(value);
}