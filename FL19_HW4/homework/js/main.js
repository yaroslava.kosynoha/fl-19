const url = 'https://jsonplaceholder.typicode.com/users';
let loadJS = document.getElementsByClassName('load-js')[0];
loadJS.addEventListener('click', () => {
    const xhr = new XMLHttpRequest();
    xhr.addEventListener('load', objNames);
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.send();

    function objNames() {
        let users = xhr.response;
        users.forEach(user => {
            let p = document.createElement('p');
            p.innerHTML = user.name;
            document.getElementsByClassName('field-js')[0].appendChild(p);
        })
    }
});

let loadFetch = document.getElementsByClassName('load-fetch')[0];
loadFetch.addEventListener('click', () => {
    fetch(url)
    .then(response => response.json())
    .then(result => {
        result.forEach(user => {
            let div = document.createElement('div');
            div.classList.add('fetch-field_items');
            div.innerHTML = `<div class='p'><p class='name'>${user.name}</p></div>
            <div class='buttons'>
            <button class='edit'>Edit</button>
            <button class='del'>Delete</button>
            </div>
            <div class='edit-field hidden'>
            <input type='text' id='name1'>
            <button class='save'>Save</button>
            </div>`;
            document.getElementsByClassName('field-fetch')[0].appendChild(div);
        }); 
        let editButton = document.getElementsByClassName('edit')[0];
        let hidden = document.getElementsByClassName('hidden')[0];

        editButton.addEventListener('click', () => {
            hidden.classList.remove('hidden');
        });

        result.forEach(id => {
            let saveButton = document.getElementsByClassName('save')[0];
            saveButton.addEventListener('click', () => {
                let input = document.getElementById('name1').value;
                fetch(`https://jsonplaceholder.typicode.com/users/${id.id}`,{
                    method:  'PUT',
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      name: input
                    })
                })
                .then(response => response.json())
                .then(name => {
                    let p = document.getElementsByClassName('name')[0];
                    p.innerHTML = name.name;
                });
            });
        });
            let delButton = document.getElementsByClassName('del')[0];
            delButton.addEventListener('click', () => {
                fetch(`https://jsonplaceholder.typicode.com/users/${result.id}`, {
                    method: 'DELETE'
                })
                .then(res => res.json())
                alert(`User with id – ${result.id} was deleted`) 
                let fetchField = document.getElementsByClassName('fetch-field_items')[0];
                fetchField.classList.add('hidden');
                
            });    
    });
});
