function extendSource(source, defaults) {
    let property;
    for (property in defaults) {
        if (source.hasOwnProperty(property) === false) {
            source[property] = defaults[property];
        }
    }
    return source;
}
export { extendSource };