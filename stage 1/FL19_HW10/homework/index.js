const start = document.getElementById('start');
const bestResult = document.getElementById('bestResult');
const bestResultEver = document.getElementById('bestResultEver');
const clearBest = document.getElementById('clearBest');
const clearBestEver = document.getElementById('clearBestEver');
const clickButton = document.getElementById('click');
let clicks;
const timer = 5000;

start.addEventListener('click', function validateNickname() {
    clicks = 0;
    const nickname = document.getElementById('nickname').value;
    try {
        if (nickname.length === 0) {
            throw new Error('Empty nickname');
        } else {
            setTimeout(() => {
                alert(`You clicked ${clicks} times`);
                localStorage.setItem(nickname, clicks);
                const highscore = localStorage.getItem('Best_res_all') || 0;
                const yhighscore = sessionStorage.getItem('Best_result') || 0;
                if (clicks > highscore) {
                    localStorage.setItem('Best_res_all', clicks);      
                }
                if (clicks > yhighscore) {
                    sessionStorage.setItem('Best_result', clicks);
                }
                clicks = 0;
            }, timer);
        }
    } catch (error) {
        alert(error.toString());
    }
});

clickButton.addEventListener('click', function onClick() {
    clicks++;
    document.getElementById('click').innerHTML = clicks;
});

bestResult.addEventListener('click', function bestRes() {
    const best = sessionStorage.getItem('Best_result');
    alert(`Best result is ${best}`);
});

bestResultEver.addEventListener('click', function bestResEver() {
    const totalBest = localStorage.getItem('Best_res_all');
    alert(`Best result for all time ${totalBest}`);
});

clearBest.addEventListener('click', function resetCounter() {
    clicks = 0;
    sessionStorage.setItem('Best_result', '0');
    alert('Best result is cleared');
});

clearBestEver.addEventListener('click', function resetCounterEver() {
    clicks = 0;
    localStorage.setItem('Best_res_all', '0');
    alert('Best result for the whole time is cleared');
});