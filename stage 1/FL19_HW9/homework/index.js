// #1
function calculateSum(arr) {
  return arr.reduce((total, a) => total + a, 0);
}

//console.log(calculateSum([1,2,3,4,5])); //15

// #2
function isTriangle(a, b, c) {
    if (a + b < c || a + c < b || b + c < a) {
        return false;
    } else {
        if (a < 0 || b < 0 || c < 0) {
            return false;
        } else {
            return true;
        }
    }
}

//console.log(isTriangle(5,6,7)); //true
//console.log(isTriangle(2,9,3)); //false

// #3
function isIsogram(word) {
    return !word.match(/([a-z]).*\1/i);
}

//console.log(isIsogram('Dermatoglyphics')); //true
//console.log(isIsogram('abab')); //false

// #4
function isPalindrome(word) {
    return word === word.split('').reverse().join('');
}

//console.log(isPalindrome('Dermatoglyphics')); //false
//console.log(isPalindrome('abbabba')); //true

// #5
function showFormattedDate(dateObj) {
    let date = new Date(dateObj);
    let month = date.toLocaleString('en', { month: 'long' });
    let day = date.getDate();
    let year = date.getUTCFullYear();
    return `${day} of ${month} ${year}`;
}

//console.log(showFormattedDate(new Date('05/12/22'))); //'12 of May, 2022'

// #6
const letterCount = (str, letter) => { 
    let letterCount = 0;
    for (let position = 0; position < str.length; position++) {
        if (str.charAt(position) === letter) {
            letterCount += 1;
        }
    }
    return letterCount;
}

//console.log(letterCount('abbaba', 'b')); //3

// #7
function countRepetitions(arr) {
    let objects = {}; 
    arr.forEach(object => { 
        objects[object] = (objects[object] || 0) + 1; 
    }); 
    return objects;
}


//console.log(countRepetitions(['banana', 'apple', 'banana'])); // { banana: 2, apple: 1 }

// #8
function calculateNumber(arr) {
    return parseInt(arr.join(''),2);
}


//console.log(calculateNumber([0, 1, 0, 1])); //5
//console.log(calculateNumber([1, 0, 0, 1])); //9
