import { dictionary } from './dictionary.js';

const guesses = 6;
let guessesLeft = guesses;
let curGuess = [];
let nextLetter = 0;
let guessedRow = dictionary[Math.floor(Math.random() * dictionary.length)];

console.log(guessedRow);

function gameBoard() {
    let board = document.getElementById('board');

    for (let i = 0; i < guesses; i++) {
        let row = document.createElement('div');
        row.className = 'row';
        
        for (let j = 0; j < 5; j++) {
            let cell = document.createElement('div');
            cell.className = 'cell';
            row.appendChild(cell);
        }

        board.appendChild(row);
    }
}

function delLetter () {
    let row = document.getElementsByClassName('row')[6 - guessesLeft];
    let cell = row.children[nextLetter - 1];
    cell.textContent = '';
    cell.classList.remove('filled-cell');
    curGuess.pop();
    nextLetter -= 1;
}

function checkWord () {
    let guessString = '';
    let rightGuess = Array.from(guessedRow);

    for (const val of curGuess) {
        guessString += val;
    }

    if (guessString.length !== 5) {
        alert('Not enough letters!');
        return;
    }

    if (!dictionary.includes(guessString)) {
        alert('Word not in list!');
        return;
    }

    
    for (let i = 0; i < 5; i++) {
        let letterPosition = rightGuess.indexOf(curGuess[i]);
        let letterColor = '';
        let row = document.getElementsByClassName('row')[6 - guessesLeft];
        let cell = row.children[i];
        if (letterPosition === -1) {
            letterColor = 'grey';
        } else {
            if (curGuess[i] === rightGuess[i]) {
                letterColor = 'green';
            } else {
                letterColor = 'yellow';
            }
            rightGuess[letterPosition] = '#';
        }
        cell.style.backgroundColor = letterColor;
    }
    if (guessString === guessedRow) {
        alert('Congratulations! You won.');
        guessesLeft = 0;
        return;
    } else {
        guessesLeft -= 1;
        curGuess = [];
        nextLetter = 0;

        if (guessesLeft === 0) {
            alert('Game over.');
            return;
        }
    }
}

function insertLetter (key) {
    if (nextLetter === 5) {
        return;
    }
    key = key.toLowerCase();
    let row = document.getElementsByClassName('row')[6 - guessesLeft];
    let cell = row.children[nextLetter];
    cell.textContent = key;
    cell.classList.add('filled-cell');
    curGuess.push(key);
    nextLetter += 1;
}

document.addEventListener('keyup', (e) => {

    if (guessesLeft === 0) {
        return;
    }
    let key = String(e.key)
    if (key === 'Backspace' && nextLetter !== 0) {
        delLetter();
        return;
    }

    if (key === 'Enter') {
        checkWord();
        return;
    }

    let found = key.match(/[А-ЩЬЮЯҐЄІЇа-щьюяґєії]/gi)
    if (!found || found.length > 1) {
        return;
    } else {
        insertLetter(key);
    }
});
function reset() {
    document.location.reload();
}
document.getElementsByClassName('buttons').addEventListener('click', (e) => {
    if (e === 'Reset') {
    reset();
    }
    if (e === 'Check') {
        checkWord();
    }
});


gameBoard();