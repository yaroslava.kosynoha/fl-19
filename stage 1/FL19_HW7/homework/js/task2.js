let userNumber;
let minNumber = 0;
let maxNumber = 8;
let maxAttempts = 3;
let maxPrize = 100;
let totalPrize = 0;
let factor = 2;
let j = 1;
let restart = true;
let notrestart = false;

window.onload = function() {
    let startGame = confirm('Do you want to play?');
    if (!startGame) {
        sayGoodBye();
    }
    while (startGame) {
        let gameNumber = getRandomInt(minNumber, maxNumber);
        let startGame = userWantsToPlay(gameNumber, maxAttempts);
        if (!startGame) {
            alert(`Thank you for your participation. Your prize is: ${totalPrize} $`);
            confirm('Do you want to play again?');
            if (restart) {
                userWantsToPlay();
            } else {
                break;
            }
        }
    }
}
function sayGoodBye() {
    alert('You did not become a billionaire, but can.');
}
function getRandomInt(minNumber, maxNumber) {
    return Math.floor(Math.random() * (maxNumber - minNumber + j) + minNumber);
}
function userWantsToPlay(gameNumber, maxAttempts) {
    let result = guessNumber(gameNumber, maxAttempts);
    if (result >= 0) {
        let range = 4;
        let prizeWon = maxPrize / Math.pow(factor, result);
        alert(`Congratulation, you won! Your prize is: ${prizeWon}$.`);
        totalPrize += prizeWon;
        let userWantsToPlay = confirm('Do you want to continue?');
        if (!userWantsToPlay) {
            alert(`Thank you for your participation. Your prize is: ${totalPrize} $`);
            return false;
        }
        max += range;
        maxPrize *= factor;
        return userWantsToPlay; 
    }
}
function guessNumber(gameNumber, maxAttempts) {
    for (let i = maxAttempts; i > 0; i--) {
        userNumber = +prompt(`Choose a pocket number from ${minNumber} to ${maxNumber}
                    Attempts left: ${i}
                    Total prize: ${totalPrize}$
                    Possible prize on the current attempt: ${maxPrize / Math.pow(x, maxAttempts - i)}$ `);

        if (userNumber === gameNumber) {
            return maxAttempts - i;
        }
    }
    return -j;
}