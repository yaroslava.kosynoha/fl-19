// #1
function extractCurrencyValue(param) {
    let data = param.toString().length;
    if (data >= 16) {
        param.replace(/[A-Z]/g,'');
        param = parseInt(param);
        return BigInt(param);
    } else {
        param.replace(/\D/g,'');
        return parseInt(param);
    }
}

console.log(extractCurrencyValue('120 USD')); // 120
console.log(extractCurrencyValue('1283948234720742 EUR')); // 1283948234720742n


// #2

let object = {
    name: 'Ann',
    age: 16,
    hobbies: undefined,
    degree: null,
    isChild: false,
    isTeen: true,
    isAdult: false
}

function clearObject(obj) { 
    for (let i in obj) {
        if (!obj[i]) {
          delete obj[i];
        }
      }
    return obj;
}

console.log(clearObject(object)); // { name: 'Ann', age: 16, isTeen: true }


// #3

function getUnique(param) {
    let x = Symbol(param);
    return x;
} 

console.log(getUnique('Test')) // Symbol('Test')


// #4

function countBetweenTwoDays(startDate, endDate) {
   let start = new Date(startDate);
   let end = new Date(endDate);
   let oneDay = 1000 * 60 * 60 * 24;
   let years = end.getFullYear() - start.getFullYear();
   let diffInTime = end.getTime() - start.getTime();
   let diffInDay = Math.round(diffInTime / oneDay);
   let diffInWeek = Math.floor(diffInDay/7);
   let diffInMonth = years * 12 + (end.getMonth() - start.getMonth());
   return `The difference between dates is:${diffInDay}day(-s),${diffInWeek}week(-s),${diffInMonth}month(-s)`;
}

console.log(countBetweenTwoDays('03/22/22', '05/25/22')); // The difference between dates is: 64 day(-s), 9 week(-s), 2 month(-s)


// #5

function filterArray(arr) {
    return arr.filter((item, index) => arr.indexOf(item) === index);
}
console.log(filterArray([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]

function filterArraySet(arr) {
    return [...new Set(arr)];
}
console.log(filterArraySet([1, 2, 2, 4, 5, 5, 5, 6, 6, 7, 7, 8, 8, 8, 9])); // [1, 2, 3, 4, 5, 6, 7, 8, 9]
