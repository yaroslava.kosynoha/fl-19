function textDate(date) {
    const weekday = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    date = new Date(date);
    let weekdays = weekday[date.getDay()];
    return weekdays;
}
//console.log(textDate(new Date(Date.now())));
//console.log(textDate(new Date(2020, 9, 22)));

function getAmountDaysToNewYear() {
    let dateToday = Date.now();
    let newYear = new Date(2023, 0, 0)
    let day = 86400000;
    let leftDays = Math.floor((newYear - dateToday)/ day);
    return leftDays;
}
console.log(getAmountDaysToNewYear());

function validateAge(birthday) {
    birthday = new Date(birthday);
    let dateToday = new Date(2022, 0, 1);
    let year = 31104000000;
    let age = Math.floor((dateToday - birthday)/year);
    let validAge = 18;
    let borderAge = 17;
    if (age>=validAge) {
        console.log('Hello adventurer, you may pass!');
    } else if (borderAge<=age) {
        console.log('Hello adventurer, you are to yang for this quest wait for few more months!');
    } else if (age<borderAge) {
        console.log(`Hello adventurer, you are to yang for this quest wait for ${validAge-age} years more!`);
    }
}
//validateAge(new Date(2004, 12, 29)); 
//validateAge(new Date(2007, 12, 29)); 
//validateAge(new Date(2000, 9, 22));

const elementP = 'tag="p" class="text" style={color: #aeaeae;} value="Aloha!"'
function transformStringToHtml(string) {
    let replace=string
    .replace(/tag="p"/, '<p')
    .replace(/{color: #aeaeae;} /,'”color: #aeaeae;”')
    .replace(/value="Aloha!"/, '>Hello World!</p>')
    return replace;
}
console.log(transformStringToHtml(elementP));

function isValidIdentifier(string) {
    return /^([a-zA-Z_$][a-zA-Z0-9\\d_$]*)$/.test(string);
}
console.log(isValidIdentifier('myVar!')); // false
console.log(isValidIdentifier('myVar$')); // true
console.log(isValidIdentifier('myVar_1')); // true
console.log(isValidIdentifier('1_myVar')); // false

const testStr = 'My name is John Smith. I am 27.'; 
function capitalize(testStr) {
    return testStr.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
}
console.log(capitalize(testStr)); // "My Name Is John Smith. I Am 27."

function isValidPassword(string) {
    return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8}$/.test(string);
}
console.log(isValidPassword('agent007')); // false (no uppercase letter) 
console.log(isValidPassword('AGENT007')); // false (no lowercase letter) 
console.log(isValidPassword('AgentOOO')); // false (no numbers) 
console.log(isValidPassword('Age_007')); // false (too short) 
console.log(isValidPassword('Agent007')); // true

function bubbleSort(arr) {
    let swap = true;
    while (swap) {
        swap = false;
        arr.forEach((item, index) => {
            if (item > arr[index+1]) {
                arr[index] = arr[index + 1];
                arr[index + 1] = item;
                swap = true;
            }
        });
    }
    return arr;
}
//console.log(bubbleSort([7,5,2,4,3,9])); //[2, 3, 4, 5, 7, 9]

const inventory = [
    { name: 'milk', brand: 'happyCow', price: 2.1 }, 
    { name: 'chocolate', brand: 'milka', price: 3 }, 
    { name: 'beer', brand: 'hineken', price: 2.2 }, 
    { name: 'soda', brand: 'coca-cola', price: 1 } 
];
function sortByItem() {
    return inventory.sort((a,b) => b.name > a.name ? -1: 1);
}

console.log(sortByItem({item: 'name', array: inventory})); // will return [
    //{ "name": "beer", "brand": "hineken", "price": 2.2 },
    //{ "name": "chocolate", "brand": "milka", "price": 3 },
    //{ "name": "milk", "brand": "happyCow", "price": 2.1 },
    //{ "name": "soda", "brand": "coca-cola", "price": 1 } ]