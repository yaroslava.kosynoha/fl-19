# FL-19

HW 1 Flexbox

TASK
In this homework you should create the same page as shown in the picture (not pixel perfect) - task/example.png by using Flexbox as much, as possible.


HW 2 CSS Grid

TASK 
In this homework you should create the same page as shown in the picture (not pixel perfect) - task/design/design.png or task/design/design.pdf by using Grid as much, as possible


HW 3 OOP Intro

TASK
Warcraft 3:
* - required
  You'll need to implement 3 classes: a Game , a Unit and a Display classes. The first time You load the page you have to create a header with the name of the game and a start button that will start the game. When You press the start button, an alert should be displayed with the ‘Choose your fighter’ sentence. After the alert, a list of characters should be displayed as selectable buttons. The start button must be hidden. You need to choose a character. the second player will be a computer that will select a random character from the list after You have selected a character. Then the list of characters disappears and the ‘Fight’ button appears. Also there should be displayed two containers with the name and stats of the character. as well as his current health and what will be deducted. After You press the Fight button the characters will attack each other and take away health considering attack rate and armor. If one of the characters health falls below zero, he loses. After the match there should be displayed an alert with the victory of the player. After accepting the alert, the player must return to app to the initial state.
Game Class Description
Implementation Details:
This is the main class and this class is responsible for the user's actions. For example choose player.
Unit Class Description
Implementation Details:
This is a constructor class for unit creation and unit actions.
Display Class Description
Implementation Details:
This class is responsible for visual manipulation.


HW 4 AJAX

TASK
Use this API (https://jsonplaceholder.typicode.com/) for your calls.
1) Display two buttons (“Load by JS” and “Load by Fetch”) and two empty containers. 1) Left container:
- when user clicks on “Load by JS” button the list of users are loaded by XMLHttpRequest (GET /users);
- the items in the list are readonly (just list of items); 2) Right container:
- when user click on “Load by Fetch” button the list of users are loaded by Fetch (GET /users);
- the items have possibility to edit name and delete item;
- when editining is finished update user on the server(call PUT method) PUT/user/${id} and update name from response.
-add possibility to delete user DELETE /user/${id}. If request is finished show alert with message – “User with id – {id} was deleted”. Items can’t be deleted as jsonplaceholder doesn’t allow to change database. We just need to show loader during the request and show alert.
3) Show spinner on every request call (can be just word – “...Loading”)
4) jsonplaceholder doesn’t allow to change database, so using PUT and DELETE can be


HW 5 Frontend Optimization

TASK
Make front-end optimization for FL19 page including:
- Add “Critical CSS” to provide quick first meaningful paint:
• Copy styles from “critical-styles.css”
• Paste them in <head> as inline styles using tag <style>
- Minify and Concatenate CSS in one file (Use online tool Shrinker).
- Implement asynchronous load of CSS and JS:
• For JS use ‘defer’ or ‘async’ attributes for ‘script’ tag
 • For CSS use “media” and “onload” attributes.
- Create smaller version of all images for mobile devices. Use ‘srcset’ attribute to show
proper image depending on screen size.
- Deliver scaled image sizes for desktop and mobile devices
- Implement lazy load of images using “lazyload” plugin (lazyload.js)

HW 6 ES Next


HW 7 Tools

TASK 
Let’s implement the famous kid game! Everybody knows the rules: - Scissors beats a paper,
- Paper beats rock,
- Rock beats scissors.
- And we play up to three wins!

Interface:
- On the page, you should show game rules and heading‘Let’s play!’ - After there should be three buttons - Rock, Paper, or Scissors.
- And ‘Reset’ link.

Interaction:
- After pressing one of the buttons game is started.
- The result of every step we should show after buttons. It should look like: “Round 1, Paper vs. Rock, You’ve WON!”
or “Round 2, Rock vs. Paper, You’ve LOST!.
- And after three wins or three losses, you should show the final result who is the winner.
- Pressing reset button should clear game data and previous results on the page.
