class Factory {
    constructor() {
        this.toys = []
    }
    produce(name, price, type='plastic') {
        let newTeddy;
        let newWooden;
        let newPlastic;
        let defaultToy;
        switch(type) {
            
            case 'teddy':
                newTeddy = new Teddy(name, price, type)
                this.toys.push(newTeddy)
                return newTeddy;
            case 'wooden': 
                newWooden = new Wooden(name, price, type)
                this.toys.push(newWooden)
                return newWooden;
            case 'plastic toys':   
                newPlastic = new PlasticToys(name, price, type)
                this.toys.push(newPlastic)
                return newPlastic;
            default: 
                defaultToy = new PlasticToys(name, price, type='plastic')
                this.toys.push(defaultToy)
                return defaultToy;
        } 
    }
}
class Toy {
    constructor (name, price) {
        this.name = name;
        this.price = price;
    }
        getToyInfo() {
            return `The toy name is ${this.name}. It costs ${this.price} dollars.`;
        }
    
}

class Teddy extends Toy {
    constructor(name, price) {
        super(name, price)
        this.material = 'cotton';
        this.getMaterialInfo = function() {
            return `The toy ${this.name} was made of ${this.material}.`;
        }
    }
}
class Wooden extends Toy {
    constructor(name, price) {
        super(name, price)
        this.material = 'wood';
        if (Wooden.exists) {
            return Wooden.instance
        }
        Wooden.instance = this
        Wooden.exists = true
        this.name = name
        this.getMaterialInfo = function() {
            return `The toy ${this.name} was made of ${this.material}.`;
        }
    }
        
}
class PlasticToys extends Toy {
    constructor(name, price) {
        super(name, price)
        this.material = 'plastic';
        this.getMaterialInfo = function() {
            return `The toy ${this.name} was made of ${this.material}.`;
        }
    }
}
const toy = new Toy()
console.log(toy)
const factory = new Factory();
const teddyBear = factory.produce('Bear', 200, 'teddy');
console.log(teddyBear.getToyInfo());
console.log(teddyBear.getMaterialInfo());

const plasticCar = factory.produce('Car', 100);
console.log(plasticCar.getToyInfo());
console.log(plasticCar.getMaterialInfo());

const plasticBear = factory.produce('Bear', 150, 'plastic');
console.log(plasticBear.getToyInfo());
console.log(plasticBear.getMaterialInfo());

const woodenHorse = factory.produce('Horse', 150, 'wooden');
console.log(woodenHorse.getToyInfo());

const woodenBear = factory.produce('Bear', 200, 'wooden');
console.log(woodenBear.getToyInfo());


class Car {
    constructor(name, host) {
        this.name = name
        this.host = host
    }
    carSound() {
        return 'Usual car sound.'
    }
}
class AmbulanceCar {
    constructor(car) {
 this.car = car 
}
    ambulanceSound() {
        return 'Siren sound.'
    }
} 

const mercedes = new Car('Mercedes', 'Doctor')
const ambulanceMercedes = new AmbulanceCar(mercedes);
console.log(ambulanceMercedes.ambulanceSound());

const toyota = new Car('Toyota', 'Doctor2')
const ambulanceToyota = new AmbulanceCar(toyota);
console.log(ambulanceToyota.ambulanceSound());