import resultField from './result';
import reset from './index';
let buttons = [];
let userCount = 0;
let aiCount = 0;

let round = 0;
buttons = document.querySelectorAll('.button');

reset.addEventListener('click', () => {
    window.location.reload();
})

buttons.forEach(elem => {
    elem.addEventListener('click', () => {
        round++;
        mainRules(elem);
        if (round === 5) {
            endGame();
        }
    });
})

function mainRules (elem) {
    let variants = ['rock', 'paper', 'scissors'];
    let choiseNum = Math.floor(Math.random()*3);
    let aiChoise = variants[choiseNum];
    let playerChoice = elem.innerText.toLowerCase();
    if (playerChoice === aiChoise) {
        resultField.innerHTML = `<p>Round ${round}, ${playerChoice} vs ${aiChoise}. Tie</p>`;
    } else if (playerChoice === 'rock' && aiChoise === 'paper') {
        resultField.innerHTML = `<p>Round ${round}, ${playerChoice} vs ${aiChoise}. You’ve LOST!</p>`
        aiCount++;
    } else if (playerChoice === 'rock' && aiChoise === 'scissors') {
        resultField.innerHTML = `<p>Round ${round}, ${playerChoice} vs ${aiChoise}. You’ve WON!</p>`
        userCount++;
    } else if (playerChoice === 'paper' && aiChoise === 'rock') {
        resultField.innerHTML = `<p>Round ${round}, ${playerChoice} vs ${aiChoise}. You’ve WON!</p>`
        aiCount++;
    } else if (playerChoice === 'paper' && aiChoise === 'scissors') {
        resultField.innerHTML = `<p>Round ${round}, ${playerChoice} vs ${aiChoise}. You’ve LOST!</p>`
        userCount++;
    } else if (playerChoice === 'scissors' && aiChoise === 'rock') {
        resultField.innerHTML = `<p>Round ${round}, ${playerChoice} vs ${aiChoise}. You’ve LOST!</p>`
        aiCount++;
    } else if (playerChoice === 'scissors' && aiChoise === 'paper') {
        resultField.innerHTML = `<p>Round ${round}, ${playerChoice} vs ${aiChoise}. You’ve WON!</p>`
        userCount++;
    }
}

function endGame() {
    if (userCount === 3) {
        resultField.innerHTML = `<p>You’ve WON!</p>`
    } else if(aiCount === 3) {
        resultField.innerHTML = `<p>You’ve LOST!</p>`
    }
}